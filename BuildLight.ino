#include <Adafruit_NeoPixel.h>
#include <avr/power.h>
#include "Color.h"
#include <Bounce2.h>

#define PIN 2
#define BUTTON_PIN 3
#define NUM_PIXELS 16

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_PIXELS, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

Bounce button = Bounce();
uint8_t state = 0;

void setup() {
  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  strip.begin();
  strip.show();
  button.attach(BUTTON_PIN);
  button.interval(30);
}

void loop() {
  if (button.update() && button.read() == LOW) {
    state = ++state % 4;
  }
  switch (state) {
    case 3: rainbowCycle(); break;
    case 1: chase(Color(0, 0, 255)); break;
    case 2: chase(Color(255, 255, 0)); break;
    case 0: spinner(strip.Color(0, 255, 0)); break;
  }
  delay(30);
}

void chase(Color c) {
  static uint8_t j=0;
  strip.clear();
  for(uint8_t i=0; i<4; ++i) {
    uint8_t px = ((int)j + i) % strip.numPixels();
    strip.setPixelColor(px, c.flatten());
  }
  strip.show();
  j = ++j % strip.numPixels();
}

void spinner(uint32_t c) {
  static uint8_t
    offset = 0, progress = 0,
    offset_i = 0;
  strip.clear();
  if (progress < NUM_PIXELS) {
    for (uint8_t i = offset; i < offset + progress; ++i) {
      strip.setPixelColor(i % NUM_PIXELS, c);
    }
  } else {
    for (uint8_t i = offset + (progress % NUM_PIXELS); i < offset + NUM_PIXELS; ++i) {
      strip.setPixelColor(i % NUM_PIXELS, c);
    }
  }
  strip.show();
  if (++offset_i > 5) {
    offset_i = 0;
    offset = ++offset % NUM_PIXELS;
  }
  progress = ++progress % (NUM_PIXELS * 2);
}



// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle() {
  static uint16_t j = 0;

  for(uint8_t i=0; i< strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
  }
  strip.show();
  j = ++j % 256;
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}

