#ifndef COLOR_H
#define COLOR_H

inline uint8_t interp(int a, int b, float p) {
  return a + (uint8_t)((b - a) / p);
}

struct Color {
  uint8_t r, g, b;
  Color(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {}
  Color operator/(const float& rhs) {
    return Color((float)r / rhs, (float)g / rhs, (float)b / rhs);
  }
  uint32_t flatten() {
    return Adafruit_NeoPixel::Color(r, g, b);
  }
  static Color interpolate(const Color& start, const Color& stop, float percent) {
    return Color(
      interp(start.r, stop.r, percent),
      interp(start.g, stop.g, percent),
      interp(start.b, stop.b, percent)
    );
  }
};

const Color BLACK(0, 0, 0);
const Color BLUE(0, 0, 255);

#endif
